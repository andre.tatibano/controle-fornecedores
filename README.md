## Controle de Fornecedores

#### Como instalar
Clone o repositório:
`git clone https://gitlab.com/andre.tatibano/controle-fornecedores.git`

O projeto foi feito usando o docker, então:
 - Entrar na pasta `controle-fornecedores`
 - Rodar o comando `docker-compose up -d`
 - Remova o `.example` do arquivo `.env.example`
 - Execute o comando `docker exec -it app php artisan migrate`
 - Entre no link `http://localhost`

#### O projeto está hospedado em um droplet da digitalOcean pelo "link"(somente ip) http://137.184.5.75/suppliers

#### Endpoints disponíveis para fornecedores via GET:
 - http://localhost/api/suppliers:
   - Lista todos os fornecedores cadastrados.
 - http://localhost/api/suppliers?name=Campos&per_page=2
   - Lista todos os fornecedores com nome "Campos", 2 por página.
 - http://localhost/api/suppliers?cpf=18667057096&per_page=2
   - Lista todos os fornecedores com o CPF "18667057096", 2 por página.
 - http://localhost/api/suppliers?cnpj=77885432000151&per_page=2
   - Lista todos os fornecedores com CNPJ "77885432000151", 2 por página.
 - http://localhost/api/suppliers?created_at=2021-08-19&per_page=2
   - Lista todos os fornecedores criados no dia "2021-08-19" (formato AAAA-MM-DD).
 - http://localhost/api/suppliers/1
   - Mostra o fornecedor com ID "1" e seus telefones cadastrados.

#### Endpoints disponíveis para fornecedores via PATCH para atualização:
 - http://localhost/api/suppliers/1

Atualiza o fornecedor cadastrado via CNPJ com ID "1", também pode atualizar somente os telefones se necessário.
 ```
 {
    "company_id": 1,
    "name": "Andre Campos",
    "CNPJ": 26514134000114,
    "phone": [
        25288555454,
        85745454557
    ]
}
```

Atualiza o fornecedor cadastrado via CPF com ID "1", também pode atualizar somente os telefones se necessário.
```
{
    "company_id": 1,
    "name": "André Campos",
    "CPF": 18667057096,
    "RG": 583048562,
	"birthdate": "2000-02-24",
	"phone": [
		12121212121,
		15123213124
	]
}
```

#### Endpoints disponíveis para fornecedores via POST para criação de fornecedores:
 - http://localhost/api/suppliers

Cria o fornecedor cadastrado via CNPJ, também adiciona os telefones ao novo fornecedor.
```
{
    "company_id": 1,
    "name": "André Campos",
    "CNPJ": 77885432000151,
	"phone": [
		12121212121,
		15123213124
	]
}
```

Cria o fornecedor cadastrado via CPF, também adiciona os telefones ao novo fornecedor.
```
{
    "company_id": 1,
    "name": "André Campos",
    "CPF": 94827732094,
    "RG": 583048562,
	"birthdate": "2000-02-24",
	"phone": [
		15123213124,
		15884234214
	]
}
```

#### Endpoints disponíveis para fornecedores via DELETE para remoção:
 - http://localhost/api/suppliers/2
   - Deleta o fornecedor com ID "2".

#### Endpoints disponíveis para empresas via GET:
 - http://localhost/api/companies
   - Lista todas as empresas cadastradas.
 - http://localhost/api/companies/1
   - Mostra a empresa com ID "1" e seus fornecedores cadastrados.

#### Endpoints disponíveis para empresas via PATCH para atualização:
 - http://localhost/api/companies/1

Atualiza a empresa com ID "1".
```
{
    "state_abbr": "SP",
    "name": "Uma Outra COmpania",
    "CNPJ": 39505177000185
}
```

#### Endpoints disponíveis para empresas via POST para criação de empresas:
 - http://localhost/api/companies

Cria uma empresa cadastrando seu estado(abreviado), nome e CNPJ.
```
{
    "state_abbr": "SP",
    "name": "Uma Outra COmpania",
    "CNPJ": 60149028000117
}
```

#### Endpoints disponíveis para empresas via DELETE para remoção:
 - http://localhost/api/companies/2
   - Deleta o fornecedor com ID "2".


Copie o conteudo abaixo e cole dentro do insomnia para ter acesso a todas as requisições:
```
{"_type":"export","__export_format":4,"__export_date":"2021-08-20T12:17:43.467Z","__export_source":"insomnia.desktop.app:v2021.3.0","resources":[{"_id":"req_d05143c9756b40fb9d235e7101392638","parentId":"fld_874ee5407e584dac9b875bfa8e0399e1","modified":1629406277405,"created":1629406253318,"url":"http://localhost/api/suppliers","name":"get suppliers","description":"","method":"GET","body":{},"parameters":[],"headers":[],"authentication":{},"metaSortKey":-1629399416802,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"fld_874ee5407e584dac9b875bfa8e0399e1","parentId":"fld_692c2f34e6d84601ae559621b2760af8","modified":1629406246144,"created":1629406246144,"name":"supplier","description":"","environment":{},"environmentPropertyOrder":null,"metaSortKey":-1629406246144,"_type":"request_group"},{"_id":"fld_692c2f34e6d84601ae559621b2760af8","parentId":"wrk_b840ab40a08d4da9837fdd8e110cc98b","modified":1629396007538,"created":1629396007538,"name":"controle fornecedores","description":"","environment":{},"environmentPropertyOrder":null,"metaSortKey":-1629396007538,"_type":"request_group"},{"_id":"wrk_b840ab40a08d4da9837fdd8e110cc98b","parentId":null,"modified":1609855598753,"created":1609855598753,"name":"Insomnia","description":"","scope":"collection","_type":"workspace"},{"_id":"req_366707fc3826437b82bde4b8a0e868d4","parentId":"fld_874ee5407e584dac9b875bfa8e0399e1","modified":1629460724502,"created":1629459619047,"url":"http://localhost/api/suppliers?name=Campos&per_page=2","name":"get suppliers filter by name","description":"","method":"GET","body":{},"parameters":[],"headers":[],"authentication":{},"metaSortKey":-1629399416789.5,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"req_4a6f3999ed584d3cb2c6ca36b2747e21","parentId":"fld_874ee5407e584dac9b875bfa8e0399e1","modified":1629460587687,"created":1629459664157,"url":"http://localhost/api/suppliers?cpf=41848178824&per_page=2","name":"get suppliers filter by CPF","description":"","method":"GET","body":{},"parameters":[],"headers":[],"authentication":{},"metaSortKey":-1629399416783.25,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"req_2661493b3836499ca0f0c4950b4b59e6","parentId":"fld_874ee5407e584dac9b875bfa8e0399e1","modified":1629460586353,"created":1629459692913,"url":"http://localhost/api/suppliers?cnpj=77885432000151&per_page=2","name":"get suppliers filter by CNPJ","description":"","method":"GET","body":{},"parameters":[],"headers":[],"authentication":{},"metaSortKey":-1629399416780.125,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"req_5b6f35cc0312470e993f198a4150b0b5","parentId":"fld_874ee5407e584dac9b875bfa8e0399e1","modified":1629460576488,"created":1629459728533,"url":"http://localhost/api/suppliers?created_at=2021-08-19&per_page=2","name":"get suppliers filter by creation_date","description":"","method":"GET","body":{},"parameters":[],"headers":[],"authentication":{},"metaSortKey":-1629399416778.5625,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"req_bf41bdc7c4a84d2ab5c0aba032eabd22","parentId":"fld_874ee5407e584dac9b875bfa8e0399e1","modified":1629407211408,"created":1629407086631,"url":"http://localhost/api/suppliers/1","name":"get supplier","description":"","method":"GET","body":{},"parameters":[],"headers":[],"authentication":{},"metaSortKey":-1629399416752,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"req_110435659e4d4f4cbc99a6bd640911d5","parentId":"fld_874ee5407e584dac9b875bfa8e0399e1","modified":1629456350989,"created":1629407187326,"url":"http://localhost/api/suppliers/1","name":"update supplier CPNJ with phone","description":"","method":"PATCH","body":{"mimeType":"application/json","text":"{\n  \"company_id\": 1,\n  \"name\": \"André Campos\",\n  \"CNPJ\": 26514134000114,\n\t\"phone\": [\n\t\t25288555454,\n\t\t85745454557\n\t]\n}"},"parameters":[],"headers":[{"name":"Content-Type","value":"application/json","id":"pair_0bcd2db7c22346408086e1ba96e5a760"}],"authentication":{},"metaSortKey":-1629399416702,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"req_69974ebbe36940168be3eaea85e52ecb","parentId":"fld_874ee5407e584dac9b875bfa8e0399e1","modified":1629456314551,"created":1629412826794,"url":"http://localhost/api/suppliers/1","name":"update supplier CPF with phone","description":"","method":"PATCH","body":{"mimeType":"application/json","text":"{\n  \"company_id\": 1,\n  \"name\": \"André Campos\",\n  \"CPF\": 41848178824,\n  \"RG\": 493086626,\n\t\"birthdate\": \"2000-02-24\",\n\t\"phone\": [\n\t\t12121212121,\n\t\t15123213124\n\t]\n}"},"parameters":[],"headers":[{"name":"Content-Type","value":"application/json","id":"pair_0bcd2db7c22346408086e1ba96e5a760"}],"authentication":{},"metaSortKey":-1629399416677,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"req_1f6ca4fbe83c48cc8e6106f34cbdd83c","parentId":"fld_874ee5407e584dac9b875bfa8e0399e1","modified":1629422465782,"created":1629407408845,"url":"http://localhost/api/suppliers","name":"create supplier CNPJ","description":"","method":"POST","body":{"mimeType":"application/json","text":"{\n  \"company_id\": 1,\n  \"name\": \"André Campos\",\n  \"CNPJ\": 77885432000151,\n\t\"phone\": [\n\t\t12121212121,\n\t\t15123213124\n\t]\n}"},"parameters":[],"headers":[{"name":"Content-Type","value":"application/json","id":"pair_aa3e1e9d346848fdbe3d62e5d20a3e31"}],"authentication":{},"metaSortKey":-1629399416652,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"req_e911a4e3c81e4a89a128b05a2c7cc044","parentId":"fld_874ee5407e584dac9b875bfa8e0399e1","modified":1629458181199,"created":1629408286196,"url":"http://localhost/api/suppliers","name":"create supplier CPF","description":"","method":"POST","body":{"mimeType":"application/json","text":"{\n  \"company_id\": 1,\n  \"name\": \"André Campos\",\n  \"CPF\": 94827732094,\n  \"RG\": 493086626,\n\t\"birthdate\": \"2000-02-24\",\n\t\"phone\": [\n\t\t15997621609,\n\t\t15884234214\n\t]\n}"},"parameters":[],"headers":[{"name":"Content-Type","value":"application/json","id":"pair_aa3e1e9d346848fdbe3d62e5d20a3e31"}],"authentication":{},"metaSortKey":-1628225477826,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"req_f86ef3f2820444b8913799056fe58b59","parentId":"fld_874ee5407e584dac9b875bfa8e0399e1","modified":1629408756147,"created":1629408742090,"url":"http://localhost/api/suppliers/2","name":"delete supplier","description":"","method":"DELETE","body":{},"parameters":[],"headers":[],"authentication":{},"metaSortKey":-1628225477776,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"req_2f8cdd51d6ea433e814a83d109b18206","parentId":"fld_c0fd87579bf54832b84a66cd22789469","modified":1629406165650,"created":1629403083140,"url":"http://localhost/api/companies","name":"get companies","description":"","method":"GET","body":{},"parameters":[],"headers":[],"authentication":{},"metaSortKey":-1629399416827,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"fld_c0fd87579bf54832b84a66cd22789469","parentId":"fld_692c2f34e6d84601ae559621b2760af8","modified":1629406156022,"created":1629406156022,"name":"company","description":"","environment":{},"environmentPropertyOrder":null,"metaSortKey":-1629406156022,"_type":"request_group"},{"_id":"req_918853992e8b460eafd71eb444bed679","parentId":"fld_c0fd87579bf54832b84a66cd22789469","modified":1629425115405,"created":1629399416677,"url":"http://localhost/api/companies/1","name":"get company","description":"","method":"GET","body":{},"parameters":[],"headers":[],"authentication":{},"metaSortKey":-1629399416777,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"req_9e567b55efaa4c45bda95cb7ec398066","parentId":"fld_c0fd87579bf54832b84a66cd22789469","modified":1629425101735,"created":1629405792170,"url":"http://localhost/api/companies/1","name":"update company","description":"","method":"PATCH","body":{"mimeType":"application/json","text":"{\n  \"state_abbr\": \"SP\",\n  \"name\": \"Uma Outra COmpania\",\n  \"CNPJ\": 39505177000185\n}"},"parameters":[],"headers":[{"name":"Content-Type","value":"application/json","id":"pair_0bcd2db7c22346408086e1ba96e5a760"}],"authentication":{},"metaSortKey":-1629399416752,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"req_6cd0fe28e6ec45a7bde514e6e2e7cc9f","parentId":"fld_c0fd87579bf54832b84a66cd22789469","modified":1629420500159,"created":1629401651599,"url":"http://localhost/api/companies","name":"create company","description":"","method":"POST","body":{"mimeType":"application/json","text":"    {\n      \"state_abbr\": \"SP\",\n      \"name\": \"Uma Outra COmpania\",\n      \"CNPJ\": 60149028000117\n    }"},"parameters":[],"headers":[{"name":"Content-Type","value":"application/json","id":"pair_aa3e1e9d346848fdbe3d62e5d20a3e31"}],"authentication":{},"metaSortKey":-1629399416739.5,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"req_db44621f5f89411789146d863d2637a8","parentId":"fld_c0fd87579bf54832b84a66cd22789469","modified":1629406162793,"created":1629405553419,"url":"http://localhost/api/companies/2","name":"delete company","description":"","method":"DELETE","body":{},"parameters":[],"headers":[],"authentication":{},"metaSortKey":-1629399416733.25,"isPrivate":false,"settingStoreCookies":true,"settingSendCookies":true,"settingDisableRenderRequestBody":false,"settingEncodeUrl":true,"settingRebuildPath":true,"settingFollowRedirects":"global","_type":"request"},{"_id":"env_d1b97ec0a10a77db20500f934aae6468f75d589a","parentId":"wrk_b840ab40a08d4da9837fdd8e110cc98b","modified":1609855598805,"created":1609855598805,"name":"Base Environment","data":{},"dataPropertyOrder":null,"color":null,"isPrivate":false,"metaSortKey":1609855598805,"_type":"environment"},{"_id":"jar_d1b97ec0a10a77db20500f934aae6468f75d589a","parentId":"wrk_b840ab40a08d4da9837fdd8e110cc98b","modified":1629461803605,"created":1609855598808,"name":"Default Jar","cookies":[],"_type":"cookie_jar"},{"_id":"spc_dfa3a94e76234e79b8770b453ab97148","parentId":"wrk_b840ab40a08d4da9837fdd8e110cc98b","modified":1609855598754,"created":1609855598754,"fileName":"Insomnia","contents":"","contentType":"yaml","_type":"api_spec"},{"_id":"env_fbc0f242de1c40019c56f2e3d7abc26f","parentId":"env_d1b97ec0a10a77db20500f934aae6468f75d589a","modified":1629395936114,"created":1629395924828,"name":"controle fornecedores","data":{},"dataPropertyOrder":null,"color":null,"isPrivate":false,"metaSortKey":1629395924828,"_type":"environment"}]}
```
