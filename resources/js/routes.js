import Suppliers from './components/Suppliers.vue';
import AddSupplier from './components/AddSupplier.vue';
import UpdateSupplier from './components/UpdateSupplier.vue';
import Companies from './components/Companies.vue';
import AddCompany from './components/AddCompany.vue';
import UpdateCompany from './components/UpdateCompany.vue';

export const routes = [{
        name: 'suppliers',
        path: '/suppliers',
        component: Suppliers
    },
    {
        name: 'addSupplier',
        path: '/suppliers/add',
        component: AddSupplier
    },
    {
        name: 'updateSupplier',
        path: '/suppliers/:id',
        component: UpdateSupplier
    },
    {
        name: 'companies',
        path: '/companies',
        component: Companies
    },
    {
        name: 'addCompany',
        path: '/companies/add',
        component: AddCompany
    },
    {
        name: 'updateCompany',
        path: '/companies/:id',
        component: UpdateCompany
    },
];
