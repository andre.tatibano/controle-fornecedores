<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Supplier extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'name',
        'CNPJ',
        'CPF',
        'RG',
        'birthdate'
    ];

    /**
     * Get the company from this supplier.
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Get the phones from the supplier.
     */
    public function phones()
    {
        return $this->hasMany(Phone::class);
    }

    /**
     * Validate supplier data with CNPJ.
     * @param array $supplier.
     */
    public static function verify_supplier_data($supplier) {
        if (empty($supplier['CNPJ']) && empty($supplier['CPF'])) {
            return [
                'CNPJ' => 'The CPF or CNPJ must be filled in.',
                'CPF' => 'The CPF or CNPJ must be filled in.'
            ];
        }
        if (!empty($supplier['CNPJ']) && !empty($supplier['CPF'])) {
            return [
                'CNPJ' => 'Only one of the two fields can be filled.',
                'CPF' => 'Only one of the two fields can be filled.'
            ];
        }
        if (!empty($supplier['CNPJ'])) {
            if (Supplier::check_invalid_cnpj($supplier['CNPJ'])) {
                return ['CNPJ' => 'Invalid CNPJ.'];
            }
            $validator = Validator::make($supplier, [
                'company_id' => 'required',
                'name' => 'required|min:1|max:255',
                'CNPJ' => 'required|size:14',
            ]);
            if ($validator->fails()) {
                return $validator->errors();
            }else{
                return null;
            }
        }
        if (!empty($supplier['CPF'])) {
            if (Supplier::check_invalid_cpf($supplier['CPF'])) {
                return ['CPF' => 'Invalid CPF.'];
            }
            $validator = Validator::make($supplier, [
                'company_id' => 'required',
                'name' => 'required|min:1|max:255',
                'CPF' => 'required|size:11',
                'RG' => 'required|min:6|max:14',
                'birthdate' => 'required|date_format:Y-m-d'
            ]);
            if ($validator->fails()) {
                return $validator->errors();
            }
            try {
                $company_state_abbr = Company::select('state_abbr')->where('id', $supplier['company_id'])->first()->state_abbr;
            }catch (\Exception $e) {
                return null;
            }
            if ($company_state_abbr == 'PR') {
                $now = Carbon::now();
                $birthdate = Carbon::createFromFormat('Y-m-d', $supplier['birthdate']);
                $diff_in_years = $now->diffInYears($birthdate);
                if ($diff_in_years < 18) {
                    return [
                        'birthdate' => 'For companies in the state of Parana, the individual supplier must be over 18 years.'
                    ];
                }
            }
        }
        return null;
    }

    /**
     * Check if CNPJ is valid.
     *
     * @param  int  $cnpj
     * @return bool
     */
    private static function check_invalid_cnpj($cnpj)
    {
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);

        if (strlen($cnpj) != 14) {
            return true;
        }
        if (preg_match('/(\d)\1{13}/', $cnpj)) {
            return true;
        }
        for ($i = 0, $j = 5, $sum = 0; $i < 12; $i++) {
            $sum += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $rest = $sum % 11;
        if ($cnpj[12] != ($rest < 2 ? 0 : 11 - $rest)) {
            return true;
        }
        for ($i = 0, $j = 6, $sum = 0; $i < 13; $i++) {
            $sum += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $rest = $sum % 11;
        if ($cnpj[13] != ($rest < 2 ? 0 : 11 - $rest)) {
            return true;
        }
    }

    /**
     * Check if CPF is valid.
     *
     * @param  int  $cpf
     * @return bool
     */
    private static function check_invalid_cpf($cpf)
    {
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );

        if (strlen($cpf) != 11) {
            return true;
        }

        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return true;
        }

        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return true;
            }
        }
    }
}
