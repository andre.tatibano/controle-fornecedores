<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_abbr',
        'name',
        'CNPJ',
    ];

    /**
     * Get the suppliers from this company.
     */
    public function suppliers()
    {
        return $this->hasMany(Supplier::class);
    }

    /**
     * Validate company data.
     * @param array $company.
     * @param int $id.
     */
    public static function verify_company_data($company, $id = 0) {
        if (Company::check_invalid_cnpj($company['CNPJ'])) {
            return ['CNPJ' => 'Invalid CNPJ.'];
        }
        $validator = Validator::make($company, [
            'state_abbr' => [ 'required', 'size:2', 'regex:/^(AC|AL|AP|AM|BA|CE|DF|ES|GO|MA|MT|MS|MG|PA|PB|PR|PE|PI|RJ|RN|RS|RO|RR|SC|SP|SE|TO|BR)/' ],
            'name' => 'required|min:1|max:255',
            'CNPJ' => ['required', Rule::unique('companies')->ignore($id), 'size:14']
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }
        return null;
    }

    /**
     * Check if CNPJ is valid.
     *
     * @param  int  $cnpj
     * @return bool
     */
    private static function check_invalid_cnpj($cnpj)
    {
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);

        if (strlen($cnpj) != 14) {
            return true;

        }
        if (preg_match('/(\d)\1{13}/', $cnpj)) {
            return true;
        }
        for ($i = 0, $j = 5, $sum = 0; $i < 12; $i++) {
            $sum += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $rest = $sum % 11;
        if ($cnpj[12] != ($rest < 2 ? 0 : 11 - $rest)) {
            return true;
        }
        for ($i = 0, $j = 6, $sum = 0; $i < 13; $i++) {
            $sum += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $rest = $sum % 11;
        if ($cnpj[13] != ($rest < 2 ? 0 : 11 - $rest)) {
            return true;
        }
    }
}
