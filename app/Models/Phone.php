<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'supplier_id',
        'phone_number',
    ];

    /**
     * Get the supplier that owns the phone number.
     */
    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }
}
