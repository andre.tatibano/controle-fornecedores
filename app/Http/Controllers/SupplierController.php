<?php

namespace App\Http\Controllers;

use App\Models\Phone;
use App\Models\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itens_per_page = (int) request('per_page', 15);

        $query = Supplier::query();
        if ($request->has('name')) {
            $query->where('name', $request->name);
        }
        if ($request->has('cpf')) {
            $query->where('CPF', $request->cpf);
        }
        if ($request->has('cnpj')) {
            $query->where('CNPJ', $request->cnpj);
        }
        if ($request->has('created_at')) {
            $query->where('created_at', '>=', $request->created_at);
            $next_day = Carbon::createFromFormat('Y-m-d', $request->created_at)->addDay()->toDateString();
            $query->where('created_at', '<=', $next_day);
        }

        return $query->paginate($itens_per_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supplier = $request->all();
        $validation_fail = Supplier::verify_supplier_data($supplier);
        if ($validation_fail) {
            return response()->json($validation_fail, 422);
        };
        if (isset($supplier['phone'])) {
            foreach ($supplier['phone'] as $phone) {
                if (!is_int($phone)) {
                    return response()->json(['phone' => 'The phone must only contain numbers '], 422);
                }
            }
            $phones = $supplier['phone'];
            unset($supplier['phone']);
        }
        $new_supplier = Supplier::create($supplier);
        if (isset($phones)) {
            foreach($phones as $phone) {
                $phone = [
                    'supplier_id' => $new_supplier->id,
                    'phone_number' => $phone
                ];
                Phone::updateOrCreate($phone);
            }
        }
        $new_supplier->phones;
        $new_supplier->company;
        return $new_supplier;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier = Supplier::find($id);
        if (empty($supplier)) {
            return response()->json(null, 404);
        }
        $supplier->phones;
        $supplier->company;
        return $supplier;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = $request->all();
        $validation_fail = Supplier::verify_supplier_data($supplier);
        if ($validation_fail) {
            return response()->json($validation_fail, 422);
        };
        if (isset($supplier['phone'])) {
            foreach($supplier['phone'] as $phone) {
                if (!is_int($phone)) {
                    return response()->json(['phone' => 'The phone must only contain numbers '], 422);
                }
                $phone = [
                    'supplier_id' => $id,
                    'phone_number' => $phone
                ];
                Phone::updateOrCreate($phone);
            }
            unset($supplier['phone']);
        }
        if (Supplier::where('id', $id)->update($supplier) == 0) {
            return response()->json(null, 404);
        }
        $supplier = Supplier::find($id);
        $supplier->phones;
        return $supplier;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroyed_itens = Supplier::destroy($id);
        if ($destroyed_itens <= 0) {
            return response()->json(null, 404);
        }
        return response()->json(null, 200);
    }
}
