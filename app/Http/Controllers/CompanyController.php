<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itens_per_page = (int) request('per_page', 15);
        return Company::paginate($itens_per_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation_fail = Company::verify_company_data($request->all());
        if ($validation_fail) {
            return response()->json($validation_fail, 422);
        };
        $new_company = Company::updateOrCreate($request->all());
        if (is_int($new_company)) {
            return Company::find($new_company);
        }
        return $new_company;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        if (empty($company)) {
            return response()->json(null, 404);
        }
        $company->suppliers;
        return $company;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation_fail = Company::verify_company_data($request->all(), $id);
        if ($validation_fail) {
            return response()->json($validation_fail, 422);
        };
        if (Company::where('id', $id)->update($request->all()) == 0) {
            return response()->json(null, 404);
        }
        return Company::find($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroyed_itens = Company::destroy($id);
        if ($destroyed_itens <= 0) {
            return response()->json(null, 404);
        }
        return response()->json(null, 200);
    }
}
